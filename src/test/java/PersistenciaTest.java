/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Modelo.Cliente;
import Modelo.Direccion;
import Modelo.Nacionalidad;
import Modelo.Turno;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author rami_
 */
public class PersistenciaTest {
    
    @Test
    public void testVerificarCreacionCliente() {
        EntityManager manager;

        EntityManagerFactory emf;
        
        emf = Persistence.createEntityManagerFactory("app");
        manager = emf.createEntityManager();

        Cliente miCliente = new Cliente("Ramiro", "Romero");

        manager.getTransaction().begin();
        manager.persist(miCliente);
        manager.getTransaction().commit();

        Cliente otroCliente = manager.find(Cliente.class, 1);
        
        emf.close();
        manager.close();
     
        assertEquals("Ramiro", otroCliente.getNombre());
    }
    
    @Test
    public void testVerificarCreacionDireccion() {
        EntityManager manager;

        EntityManagerFactory emf;
        
        emf = Persistence.createEntityManagerFactory("app");
        manager = emf.createEntityManager();

        Cliente miCliente = new Cliente("Ramiro", "Romero");
        miCliente.setDireccion(new Direccion("Calle falsa 123", miCliente));
        miCliente.setDireccion(new Direccion("Calle falsa 1234", miCliente));

        manager.getTransaction().begin();
        manager.persist(miCliente);
        manager.getTransaction().commit();

        Cliente otroCliente = manager.find(Cliente.class, 1);
        
        emf.close();
        manager.close();
     
        assertEquals("Calle falsa 123", otroCliente.getDirecciones().get(0).getDireccion());
        assertEquals("Calle falsa 1234", otroCliente.getDirecciones().get(1).getDireccion());
    }
    
    @Test
    public void testVerificarCreacionTurnos() {
        EntityManager manager;

        EntityManagerFactory emf;
        
        emf = Persistence.createEntityManagerFactory("app");
        manager = emf.createEntityManager();

        Cliente miCliente = new Cliente("Ramiro", "Romero");
        miCliente.setDireccion(new Direccion("Calle falsa 123", miCliente));
        miCliente.setDireccion(new Direccion("Calle falsa 1234", miCliente));
        miCliente.getTurnos().add(new Turno(miCliente));
        miCliente.getTurnos().add(new Turno(miCliente));
        miCliente.getTurnos().add(new Turno(miCliente));

        manager.getTransaction().begin();
        manager.persist(miCliente);
        manager.getTransaction().commit();

        Cliente otroCliente = manager.find(Cliente.class, 1);
        
        emf.close();
        manager.close();
     
        assertEquals(3, otroCliente.getTurnos().size());
    }
    
    @Test
    public void testVerificarCreacionNacionalidad() {
        EntityManager manager;

        EntityManagerFactory emf;
        
        emf = Persistence.createEntityManagerFactory("app");
        manager = emf.createEntityManager();

        //Creo una nacionalidad
        Nacionalidad unaNacionalidad = new Nacionalidad("Argentina");
        
        //Creo un nuevo cliente
        Cliente miCliente = new Cliente("Ramiro", "Romero");
        
        //Seteo su direccion
        miCliente.setDireccion(new Direccion("Calle falsa 123", miCliente));
        miCliente.setDireccion(new Direccion("Calle falsa 1234", miCliente));
        
        //Seteo su nacionalidad
        miCliente.setMiNacionalidad(unaNacionalidad);
        unaNacionalidad.setCliente(miCliente);
        
        //Creo y asigno sus turnos
        miCliente.getTurnos().add(new Turno(miCliente));
        miCliente.getTurnos().add(new Turno(miCliente));
        miCliente.getTurnos().add(new Turno(miCliente));

        manager.getTransaction().begin();
        manager.persist(miCliente);
        manager.persist(unaNacionalidad);
        
        manager.getTransaction().commit();

        Cliente otroCliente = manager.find(Cliente.class, 1);
        
        emf.close();
        manager.close();
     
        var nacionalidad = otroCliente.getMiNacionalidad();
        String nombreNacion = nacionalidad.getNombreNacionalidad();
        
        assertEquals("Argentina", nombreNacion);
    }
    
}
