/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author rami_
 */
@Entity
public class Cliente implements Serializable{ 
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    @Basic
    private String nombre;
    @Basic
    private String apellido;

    @OneToMany(mappedBy = "miCliente", cascade = CascadeType.ALL)
    private List<Turno> turnos = new ArrayList<>();
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "cliente_direcciones",
                joinColumns = {@JoinColumn(name = "id_cliente")},
                inverseJoinColumns = {@JoinColumn(name = "id_direccion")})
    private List<Direccion> direcciones;
    
    @ManyToOne
    @JoinColumn(name = "id_nacionalidad")
    private Nacionalidad miNacionalidad;

    public Cliente() {
    }
    
    public Cliente(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
        
        this.direcciones = new ArrayList();
    }
    
    /**
     * Esta funcion se encarga de verificar si el nombre proporcionado es igual al nombre de la instancia.
     * @param nombre
     * @return Retorna VERDADERO si coinciden los nombres. En caso contrario, retorna FALSO.
     */
    public boolean isCliente(String nombre){
        boolean isCliente = false;
        
        if(this.nombre.equals(nombre)){
            isCliente = true;
        }
        
        return isCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public List<Turno> getTurnos() {
        return turnos;
    }

    public void setTurnos(List<Turno> turnos) {
        this.turnos = turnos;
    }

    public List<Direccion> getDirecciones() {
        return direcciones;
    }

    public void setDireccion(Direccion  unaDireccion) {
        this.direcciones.add(unaDireccion);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Nacionalidad getMiNacionalidad() {
        return miNacionalidad;
    }

    public void setMiNacionalidad(Nacionalidad miNacionalidad) {
        this.miNacionalidad = miNacionalidad;
    }

    @Override
    public String toString() {
        return "Cliente{" + "id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + '}';
    }
}

