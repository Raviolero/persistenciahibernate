/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 *
 * @author rami_
 */
@Entity
public class Direccion implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    
    @Basic
    private String direccion;
    
    @ManyToMany(mappedBy = "direcciones", cascade = CascadeType.ALL)
    private List<Cliente> clientes;

    public Direccion() {
    }

    public Direccion(String direccion) {
        this.direccion = direccion;
        this.clientes = new ArrayList();
    }
    
    public Direccion(String direccion, Cliente miCliente) {
        this.direccion = direccion;
        this.clientes = new ArrayList();
        
        this.clientes.add(miCliente);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    @Override
    public String toString() {
        return this.direccion;
    }
    
}
