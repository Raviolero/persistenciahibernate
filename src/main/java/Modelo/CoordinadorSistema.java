/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Controlador.ControladorLogico;
import Excepciones.ClienteExistente;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rami_
 */
public class CoordinadorSistema {

    private final ControladorLogico miControladorLogico;

    private List<Cliente> clientes;
    private List<Turno> turnos;
    private List<Nacionalidad> nacionalidades;

    public CoordinadorSistema(ControladorLogico miControladorLogico) {
        this.miControladorLogico = miControladorLogico;
        
        clientes = new ArrayList();
        nacionalidades = new ArrayList();
    }

    /**
     * Esta funcion coordina la instanciacion de un nuevo cliente con los datos
     * proporcionados.Ademas, persiste en caso de lograr instanciarlo.En caso contrario, devolvera una excepcion.
     *
     * @param nombre
     * @param apellido
     * @param direccion
     * @return Retorna el cliente creado en cuestion.
     * @throws Excepciones.ClienteExistente
     */
    public Cliente crearCliente(String nombre, String apellido, String direccion) throws ClienteExistente {
        //Creo un nuevo cliente pero no lo instacion
        Cliente nuevoCliente;
        
        //Pregunta si existe el cliente con ese nombre
        if (!clienteExistente(nombre)) {
            //En caso que no exista, puedo instanciar
            //Instancio un nuevo cliente
            nuevoCliente = new Cliente(nombre, apellido);
            
            //Creo la direccion
            nuevoCliente.setDireccion(new Direccion(direccion, nuevoCliente));
            
            //Creo dos turnos aleatorios para demostrar
            nuevoCliente.getTurnos().add(new Turno(nuevoCliente));
            nuevoCliente.getTurnos().add(new Turno(nuevoCliente));
            nuevoCliente.getTurnos().add(new Turno(nuevoCliente));
            
            //Creo una nacionalidad
            Nacionalidad unaNacionalidad = new Nacionalidad("Argentina");
            
            //Le asigno su cliente
            unaNacionalidad.setCliente(nuevoCliente);
            
            //Asigno la nueva nacionalidad al cliente
            nuevoCliente.setMiNacionalidad(unaNacionalidad);
            
            //Guardo la nueva nacionalidad en la lista del coordinador
            nacionalidades.add(unaNacionalidad);
            
            //Persisto el nuevo cliente
            miControladorLogico.persistirCliente(nuevoCliente);

            //Agrego a mi lsita de clientes
            clientes.add(nuevoCliente);
        }else{
            //En caso que ya exista se producira una exception
            throw new ClienteExistente();
        }
        
        return nuevoCliente;
    }

    /**
     * Recorre todos los clientes para preguntarles si coincide con su nombre.
     *
     * @param nombre
     * @return En caso que coincida, retornara VERDADERO. En caso contrario,
     * retornara FALSO.
     */
    private boolean clienteExistente(String nombre) {
        boolean existencia = false;

        //Recorro todos los clientes para preguntar si ya existe
        for (Cliente unCliente : clientes) {
            //Si no retorna VERDADERO, es porque no existe el cliente y puedo instanciarlo
            if (!unCliente.isCliente(nombre)) {
                existencia = true;
            }
        }

        return existencia;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public List<Turno> getTurnos() {
        return turnos;
    }

    public void setTurnos(List<Turno> turnos) {
        this.turnos = turnos;
    }
}
