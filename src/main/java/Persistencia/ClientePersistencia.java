/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Modelo.Cliente;
import javax.persistence.EntityManager;

/**
 *
 * @author rami_
 */
public class ClientePersistencia {
    /**
     * Esta funcion se encarga de persistir un objeto cliente.
     * @param unCliente
     * @param miAdministradorEntidad 
     */
    public void persistirCliente(Cliente unCliente, EntityManager miAdministradorEntidad){
        //Inicia la transaccion
        miAdministradorEntidad.getTransaction().begin();
        
        //Pregunta si tiene nacionalidad
        if(tieneNacionalidad(unCliente)){
            //En caso de tenerla, primero persiste la nacionalidad
            miAdministradorEntidad.persist(unCliente.getMiNacionalidad());
        }
        //Persiste al cliente
        miAdministradorEntidad.persist(unCliente);
        
        //Realiza el commit necesario
        miAdministradorEntidad.getTransaction().commit();
    }
    
    /**
     * Funcion encargada de saber si un cliente proporcionado tiene una nacionalidad asignada.
     * @param unCliente
     * @return Retorná VERDADERO en caso afirmativo. En caso contrario, retornará FALSO.
     */
    private boolean tieneNacionalidad(Cliente unCliente){
        return unCliente.getMiNacionalidad() != null;
    }
}
