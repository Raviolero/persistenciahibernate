/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Excepciones.ClienteExistente;
import Modelo.Cliente;
import Modelo.CoordinadorSistema;

/**
 *
 * @author rami_
 */
public class ControladorLogico {
    private final ControladorPersistencia miControladorPersistencia;
    private final CoordinadorSistema miCoordinadorSistema;

    public ControladorLogico() {
        miControladorPersistencia = new ControladorPersistencia();
        miCoordinadorSistema = new CoordinadorSistema(this);
    }
    
    /**
     * Esta funcion pide al coordinador del modelo que cree un nuevo cliente.
     * @param nombre
     * @param apellido 
     * @param direccion 
     * @return  
     * @throws Excepciones.ClienteExistente 
     */
    public Cliente crearCliente(String nombre, String apellido, String direccion) throws ClienteExistente{
        return miCoordinadorSistema.crearCliente(nombre, apellido, direccion);
    }
    
    //--------------PERSISTENCIA---------------------
    /**
     * Esta funcion se encarga de pedir al Controlador de la Persistencia que persista un nuevo cliente instanciado.
     * @param unCliente 
     */
    public void persistirCliente(Cliente unCliente){
        miControladorPersistencia.crearClientePersistencia(unCliente);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try{
            Cliente miNuevoCliente = new ControladorLogico().crearCliente("Ramiro", "Romero", "Calle falsa 123");
            
            System.out.println("Se creo exitosamente. \n\n El nuevo cliente con nombre "+ miNuevoCliente.getNombre() +" tiene direccion"
                    + " en "+ miNuevoCliente.getDirecciones().get(0) +", posee nacionalidad de "+ miNuevoCliente.getMiNacionalidad().getNombreNacionalidad() +" y posee los turnos de"
                            + miNuevoCliente.getTurnos().toString() + ".");
        }
        catch(ClienteExistente ex){
            System.err.println(ex);
        } 
    }
}
