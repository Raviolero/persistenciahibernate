/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Cliente;
import Persistencia.ClientePersistencia;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author rami_
 */
public class ControladorPersistencia {
    private final EntityManagerFactory miFabricaDeEntidades;
    private final EntityManager miAdministradorDeEntidades;
    
    private ClientePersistencia miClienteP;

    public ControladorPersistencia() {
        miFabricaDeEntidades = Persistence.createEntityManagerFactory("app");
        miAdministradorDeEntidades = miFabricaDeEntidades.createEntityManager();
        
        instanciarEntidades();
    }
    
    private void instanciarEntidades(){
        miClienteP = new ClientePersistencia();
    }
    
    public void crearClientePersistencia(Cliente unCliente){
        miClienteP.persistirCliente(unCliente, miAdministradorDeEntidades);
    }
}
